﻿using System;

namespace Machine_Learning_Eventos_Delegados {
    class MachineLearning {
        /// <summary>
        /// Ponto de entrada principal para o aplicativo.
        /// </summary>
        /// // isto é um teste @emanuel
        [STAThread]
        static void Main() {
            Controller controller = new Controller();
            controller.IniciarPrograma();
        }
    }
}
