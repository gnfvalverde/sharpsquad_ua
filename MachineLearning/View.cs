﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machine_Learning_Eventos_Delegados
{
    public class View
    {
        private Model model;
        private Form1 janela;

        public event EventHandler UtilizadorEnviaFicheiro;
        
        public delegate void SolicitacaoListaFormas(ref List<Forma> listadeformas);
        public event SolicitacaoListaFormas ProcessarFicheiroComAlgoritmo;

        internal View(Model m)
        {
            model = m;
        }

        public void AtivarInterface()
        {
            janela = new Form1();
            janela.View = this;
           
            // Desenhar interface da submissão do ficheiro - * Emanuel
            janela.ShowDialog();
        }
        
        public void SubmeterFicheiro(object origem, EventArgs e)
        {
            //Ver comentários em Form1.cs: botaoNovaForma_Click
            UtilizadorEnviaFicheiro(origem, e);
        }
        public void GerarMensagemFicheiroGuardado()
        {
            //Fazer Aparecer na 'janela'  mensagem ficheiro submetido com sucesso

        }
        public void GerarAnimacaoProcessar()
        {
            //Fazer Aparecer na 'janela'
            //uma barra ou roda animada entquanto o processamento do algoritmo decorre

        }
    }
}
