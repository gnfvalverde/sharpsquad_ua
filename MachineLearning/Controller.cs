﻿namespace Machine_Learning_Eventos_Delegados {
    class Controller {
        
        Model model;
        View view;

        public delegate void AtivacaoInterface(object origem);
        public event AtivacaoInterface AtivarInterface;
        public Controller() {
            view = new View(model);
            model = new Model(view);

            /*Ligar o evento da View ao método do Controller, de forma desacoplada
            porque a View não sabe quem responderá ao evento.*/
            view.UtilizadorEnviaFicheiro += model.GuardarFicheiro();
            view.ProcessarFicheiro += model.ProcessarFicheiro;
        }
        public void IniciarPrograma() {
            view.AtivarInterface();
        }
        public void UtilizadorEnviouFicheiro(object fonte, System.EventArgs args) {
            // Implementar...
            model.GuardarFicheiro();
        }
        public void ProcessarFicheiro()
        {
            model.ProcessarFicheiro();
        }

    }
}